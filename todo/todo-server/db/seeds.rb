# coding: utf-8
tasks = Task.create(
  [
   { title: "HTML&CSS", details: "Webページの見た目をつくる言語", deadline: "2019-08-15", priority: "High", status: "完了"}, 
   { title: "JavaScript", details: "多様な可能性を秘めたフロントエンド言語", deadline: "2019-08-16", priority: "Middle", status: "着手"}, 
   { title: "jQuery", details: "効果やアニメーションを実装できるJavaScriptライブラリ", deadline: "2019-08-17", priority: "Middle", status: "未着手"}, 
   { title: "Ruby", details: "直感的に理解しやすく、高い生産性を誇るサーバーサイド言語", deadline: "2019-07-18", priority: "Low", status: "未着手"}
  ]
)
