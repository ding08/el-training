FactoryBot.define do
  factory :task do
    title { "MyString" }
    details { "MyText" }
    deadline { "" }
    priority { "MyString" }
    status { "MyString" }
  end
end
